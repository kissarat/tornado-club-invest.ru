<?php
/**
 * @link http://zenothing.com/
 */

namespace app\modules\pyramid\controllers;


use app\behaviors\Access;
use app\helpers\SQL;
use app\modules\pyramid\models\Matrix;
use app\modules\pyramid\models\Node;
use app\modules\pyramid\models\Type;
use Yii;
use yii\data\ActiveDataProvider;
use yii\data\Pagination;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\Response;

/**
 * @author Taras Labiak <kissarat@gmail.com>
 */
class NodeController extends Controller
{
    public function behaviors() {
        return [
            'access' => [
                'class' => Access::class,
                'manager' => ['create', 'update', 'delete']
            ],

            'cache' => [
                'class' => 'yii\filters\HttpCache',
                'cacheControlHeader' => 'must-revalidate, private',
                'only' => ['index', 'matrix'],
                'enabled' => true,
                'lastModified' => function ($action, $params) {
                    $query = Node::find();
                    if (isset($params['user'])) {
                        $query->where(['user_name' => $params['user']]);
                    }

                    if (isset($params['id'])) {
                        $query->where(['type_id' => (int) SQL::queryCell('SELECT type_id FROM node WHERE id = :id', [
                            ':id' => (int) $params['id']
                        ])]);
                    }

                    return (int) $query->max('time');
                },
            ]
        ];
    }

    public function actionIndex($user = null, $id = null) {
        $parent = $id ? $this->findModel($id) : null;
        $query = Node::find()
            ->with('type');
        if ($user) {
            $query->andWhere(['user_name' => $user]);
        }
        elseif ($parent) {
            $query
                ->andWhere('"time" < :time', [':time' => $parent->time])
                ->andWhere(['type_id' => $parent->type_id]);
        }
        return $this->render('index', [
            'dataProvider' => new ActiveDataProvider([
                'query' => $query,
                'sort' => [
                    'defaultOrder' => [
                        'time' => SORT_DESC,
                        'id' => SORT_DESC,
                    ]
                ]
            ]),
            'parent' => $parent
        ]);
    }

    public function actionCreate($id = null) {
        $model = new Node();
        if ($id) {
            $base = $this->findModel($id);
            $model->time = $base->time - 1;
            $model->type_id = $base->type_id;
        }
        else {
            $model->time = time();
        }

        $is_post = $model->load(Yii::$app->request->post());
        if ($is_post) {
            $model->count = $model->type->degree;
            if ($is_post && $model->validate()) {
                if ($model->save(false)) {
                    return $this->redirect(['invest', 'id' => $model->id]);
                }
            }
            else {
                Yii::$app->session->setFlash('error', json_encode($model->errors, JSON_UNESCAPED_UNICODE));
            }
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    public function actionUpdate($id) {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['invest', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    public function actionDelete($id) {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    public function actionMatrix($page = 1, $size = 15, $order = 'asc') {
        if ((Yii::$app->user->isGuest || !Yii::$app->user->identity->isManager()) && $size > 100) {
            return $this->redirect(['index', 'size' => 15]);
        }

        $count = Matrix::find()->select('count(*) as "count"')->groupBy('type_id')->column();
        $count = empty($count) ? 0 : (int) max($count);

        $pages = [
            'totalCount' => $count,
            'pageSize' => $size,
            'pageSizeParam' => 'size',
            'page' => $page - 1
        ];

        $pages = new Pagination($pages);

        /** @var \yii\db\Query $query */
        $query = null;
        $order = 'desc' == $order ? SORT_DESC : SORT_ASC;
        $table = [];
        foreach(Type::all() as $type) {
            $table[] = Matrix::find()
                ->where(['type_id' => $type->id])
                ->offset($pages->offset)
                ->limit($pages->limit)
                ->orderBy(['time' => $order, 'id' => $order])
                ->all();
        }

        return $this->render('matrix', [
            'models' => $table,
            'pages' => $pages,
            'order' => $order
        ]);
    }

    /**
     * Finds the Type model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Node the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        if (($model = Node::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
