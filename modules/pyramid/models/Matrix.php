<?php
/**
 * @link http://zenothing.com/
 */

namespace app\modules\pyramid\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * @author Taras Labiak <kissarat@gmail.com>
 * This is the model class for table "matrix".
 *
 * @property integer $id
 * @property integer $type_id
 * @property string $user_name
 * @property integer $count
 * @property integer $reinvest_from
 * @property integer $time
 */
class Matrix extends ActiveRecord
{
    public static function tableName()
    {
        return 'matrix';
    }

    public function rules()
    {
        return [
            [['id', 'type_id', 'count', 'reinvest_from', 'time'], 'integer'],
            [['type_id', 'user_name', 'time'], 'required'],
            [['user_name'], 'string', 'max' => 24]
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'type_id' => 'Type ID',
            'user_name' => 'User Name',
            'count' => 'Count',
            'reinvest_from' => 'Reinvest From',
            'time' => 'Time',
        ];
    }
}
