<?php
/**
 * @link http://zenothing.com/
 */

use app\modules\pyramid\models\Type;
use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$description = null;

$title = Yii::t('app', 'Plans');
if (!$this->title) {
    $this->title = $title;

    $description = 'Для удобства работы в нашем проекте, администрация проекта решила создать
        три тарифных плана для потенциальных клиентов нашего проекта.
        Маркетинг план состоит из 3 активных планов для заработка в нашем проекте';

    $this->registerMetaTag([
        'name' => 'description',
        'content' => $description
    ]);
}

$columns = [
    [
        'attribute' => 'id',
        'label' => Yii::t('app', 'Name'),
        'format' => 'html',
        'value' => function(Type $model) {
            return $model->isSpecial() ? Yii::t('app', 'Team plan') : $model->getName();
        }
    ],
    [
        'attribute' => 'stake',
        'value' => function(Type $model) {
            return $model->stake ? '$' . ((int) $model->stake) : '';
        }
    ],
    [
        'attribute' => 'income',
        'value' => function(Type $model) {
            return $model->income ? '$' . ((int) $model->income) : '';
        }
    ]
];

if (!Yii::$app->user->isGuest) {
    $columns[] = [
        'label' => Yii::t('app', 'Action'),
        'format' => 'html',
        'value' => function(Type $model) {
            /** @var \app\models\User $user */
            $user = Yii::$app->user->identity;
            if ($model->isSpecial() && !$user->isTeam()) {
                return Yii::t('app', 'Team plan');
            }
            elseif ($user->account >= $model->stake) {
                return Html::a(Yii::t('app', 'Open'), ['/pyramid/type/open', 'id' => $model->id], [
                    'class' => 'btn btn-success',
                    'data' => ['method' => 'post']
                ]);
            }
            else {
                return Yii::t('app', 'Insufficient funds');
            }
        }
    ];
}
?>
<div class="type-index">
    <h1 class="bagatelle"><?= Html::encode($title) ?></h1>

    <?php
    if ($description) {
        echo Html::tag('div', $description, ['class' => 'form-group']);
    }

    echo GridView::widget([
        'dataProvider' => $dataProvider,
        'summary' => '',
        'columns' => $columns
    ]);

    if ($description) {
        echo Html::tag('div', Yii::t('app', "After two users signup and open a plan you'll receive reward"));
    }
    ?>
</div>
