<?php
/**
 * @link http://zenothing.com/
 * @var \yii\data\ActiveDataProvider $dataProvider
 * @var \app\models\User $identity
 */

use app\models\Message;
use yii\grid\GridView;
use yii\helpers\Html;

$identity = Yii::$app->user->identity;

echo GridView::widget([
    'dataProvider' => $dataProvider,
    'columns' => [
        'sender',
        'receiver',
        'content',
        [
            'label' => Yii::t('app', 'Action'),
            'format' => 'html',
            'value' => function(Message $model) {
                return Html::a(Yii::t('app', 'View'), ['dialog',
                    'user' => $model->sender == $identity->name ? $model->receiver : $model->sender
                ]);
            }
        ]
    ]
]);
