<?php
/**
 * @link http://zenothing.com/
 * @var \yii\data\ActiveDataProvider $dataProvider
 * @var \app\models\User $identity
 */

use app\models\Message;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

$identity = Yii::$app->user->identity;

$form = ActiveForm::begin();
echo implode("\n", [

]);

echo GridView::widget([
    'dataProvider' => $dataProvider,
    'columns' => [
        [
            'attribute' => 'content',
            'format' => 'html',
            'value' => function(Message $model) {
                return Html::tag('div', $model->sender) . Html::tag('div', $model->content);
            }
        ]
    ]
]);
