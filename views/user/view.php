<?php
/**
 * @link http://zenothing.com/
 */

use app\models\User;
use app\modules\pyramid\models\Type;
use yii\data\ActiveDataProvider;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\User */


$this->title = $model->name;
if (!Yii::$app->user->isGuest && Yii::$app->user->identity->isManager()) {
    $this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Users'), 'url' => ['index']];
    $this->params['breadcrumbs'][] = $this->title;
}

$columns = [
    'name',
    'email:email',
    [
        'attribute' => 'account',
        'format' => 'html',
        'value' => Html::tag('span', $model->account) . ' ' . ($model->account > 0 ? Html::a(Yii::t('app', 'Withdraw'),
                ['invoice/invoice/create', 'amount' => (int) $model->account, 'scenario' => 'withdraw'],
                ['class' => 'btn btn-success btn-xs']) : '')
    ],
    'phone',
    'skype',
    'forename',
    'surname',
    'perfect',
    [
        'attribute' => 'country',
        'format' => 'html',
        'value' => $model->country ? Html::tag('span', $model->country, ['class' => 'country']) : null
    ],
    [
        'attribute' => 'timezone',
        'format' => 'html',
        'value' => Html::tag('span', $model->timezone ?: 'Europe/Moscow', ['class' => 'timezone'])
    ],
    'duration',
];

if (!Yii::$app->user->isGuest && Yii::$app->user->identity->isAdmin()) {
    $columns[] = [
        'attribute' => 'status',
        'value' => User::statuses()[$model->status]
    ];
}

$social_url = 'http://' . $_SERVER['HTTP_HOST'];

function addtomany($network) {
    return "http://www.addtoany.com/add_to/$network?" . http_build_query([
        'linkname' => Yii::$app->name,
        'linkurl' => 'http://' . $_SERVER['HTTP_HOST']
    ]);
}
?>
<div class="user-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <div class="form-group">
        <?= Html::a(Yii::t('app', 'Opened plans'),
            ['pyramid/node/index', 'user' => $model->name], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Income'),
            ['pyramid/income/index', 'user' => $model->name], ['class' => 'btn btn-primary']) ?>
        <?php
        if (!Yii::$app->user->isGuest) {
            if ($model->name == Yii::$app->user->identity->name || Yii::$app->user->identity->isAdmin()) {
                echo Html::a(Yii::t('app', 'Change Password'),
                    ['user/password', 'name' => $model->name], ['class' => 'btn btn-warning']);
            }

            if (Yii::$app->user->identity->isManager()) {
                echo ' ' . Html::a(Yii::t('app', 'Inform'),
                        ['message/create', 'user' => $model->name], ['class' => 'btn btn-primary']);
            }
            ?>
            <?= Html::a(Yii::t('app', 'Update'), ['update', 'name' => $model->name], ['class' => 'btn btn-primary']) ?>
            <?php
            echo implode("\n", [
                Html::a(Html::img('/img/vkontakte.png', ['alt' => 'Вконтакте']),
                    "https://vk.com/share.php?" . http_build_query(['url' => $social_url])),
                Html::a(Html::img('/img/facebook.png', ['alt' => 'Facebook']),
                    'https://www.facebook.com/sharer.php?' . http_build_query([
                        's' => 100,
                        'p[url]' => $social_url
                    ])),
                Html::a(Html::img('/img/odnoklassniki.png', ['alt' => 'Одноклассники']),
                    addtomany('odnoklassniki')),
                Html::a(Html::img('/img/twitter.png', ['alt' => 'Twitter']),
                    'https://twitter.com/intent/tweet?' . http_build_query(['url' => $social_url])),
                ''
            ]);

            if (Yii::$app->user->identity->isAdmin()) {
                if (empty($model->hash)) {
                    echo Html::a(Yii::t('app', 'Activate'), ['email', 'code' => $model->name], ['class' => 'btn btn-primary']) . ' ';
                }
                echo Html::a(Yii::t('app', 'Delete'), ['delete', 'name' => $model->name
                ], [
                    'class' => 'btn btn-danger',
                    'data' => [
                        'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                        'method' => 'post',
                    ],
                ]);
            }
            elseif ($model->name == Yii::$app->user->identity->name) {
                echo implode("\n", [
                    Html::beginForm('https://perfectmoney.is/api/step1.asp', 'POST'),
                    Html::hiddenInput('PAYEE_ACCOUNT', Yii::$app->perfect->wallet),
                    Html::textInput('PAYMENT_AMOUNT', 20, ['size' => 3, 'class' => 'btn']),
                    Html::hiddenInput('PAYEE_NAME', Yii::$app->name),
                    Html::hiddenInput('PAYMENT_UNITS', 'USD'),
                    Html::hiddenInput('PAYMENT_URL', Url::to(['/invoice/invoice/success'], true)),
                    Html::hiddenInput('NOPAYMENT_URL', Url::to(['/invoice/invoice/fail'], true)),
                    Html::hiddenInput('BAGGAGE_FIELDS', 'USER_NAME'),
                    Html::hiddenInput('USER_NAME', Yii::$app->user->identity->name),
                    Html::button(Yii::t('app', 'Pay'), [
                        'name' => 'PAYMENT_METHOD',
                        'type' => 'submit',
                        'class' => 'btn'
                    ]),
                    Html::endForm()
                ]);
            }
        }
        ?>
    </div>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => $columns,
    ]) ?>

    <?= Yii::$app->view->renderFile('@app/modules/pyramid/views/type/index.php', [
        'dataProvider' => new ActiveDataProvider([
            'query' => Type::find(),
            'sort' => [
                'defaultOrder' => ['id' => SORT_ASC]
            ]
        ]),
    ]) ?>
</div>
