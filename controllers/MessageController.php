<?php
/**
 * @link http://zenothing.com/
 */

namespace app\controllers;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use app\models\Message;
use Yii;

/**
 * @author Taras Labiak <kissarat@gmail.com>
 */
class MessageController extends Controller {

    public function actionCreate($user = null) {
        return $this->edit(new Message([
            'receiver' => $user
        ]));
    }

    protected function edit(Message $model) {
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->setFlash('success', 'Message sent');
            return $this->redirect(['/home/index']);
        }

        return $this->render('edit', ['model' => $model]);
    }

    public function actionIndex() {
        /** @var \app\models\User $user */
        $user = Yii::$app->user->identity;
        return $this->render('index', [
            'dataProvider' => new ActiveDataProvider([
                'query' => Message::find()->orWhere([
                    'sender' => $user->name,
                    'receiver' => $user->name,
                ]),
                'sort' => [
                    'defaultOrder' => ['time' => SORT_DESC]
                ]
            ])
        ]);
    }

    public function actionDialog($user) {
        /** @var \app\models\User $identity */
        $identity = Yii::$app->user->identity;
        $message = new Message();
        if ($message->load(Yii::$app->request->post())) {
            $message->save();
        }
        return $this->render('dialog', [
            'dataProvider' => new ActiveDataProvider([
                'query' => Message::find()
                    ->orWhere([
                        'sender' => $identity->name,
                        'receiver' => $identity->name,
                    ])
                    ->orWhere([
                        'sender' => $user,
                        'receiver' => $user,
                    ]),
                'sort' => [
                    'defaultOrder' => ['time' => SORT_DESC]
                ]
            ])
        ]);
    }
}
